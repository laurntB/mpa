class CreateEndsteps < ActiveRecord::Migration[7.0]
  def change
    create_table :endsteps do |t|
      t.boolean :validation
      t.string :observation
      t.references :user, null: false, foreign_key: true
      t.references :instruction, null: false, foreign_key: true

      t.timestamps
    end
  end
end
