class FicheTagsController < ApplicationController
  before_action :set_fiche_tag, only: %i[ show edit update destroy ]

  # GET /fiche_tags or /fiche_tags.json
  def index
    @fiche_tags = FicheTag.all
  end

  # GET /fiche_tags/1 or /fiche_tags/1.json
  def show
  end

  # GET /fiche_tags/new
  def new
    @fiche_tag = FicheTag.new
  end

  # GET /fiche_tags/1/edit
  def edit
  end

  # POST /fiche_tags or /fiche_tags.json
  def create
    @fiche_tag = FicheTag.new(fiche_tag_params)

    respond_to do |format|
      if @fiche_tag.save
        format.html { redirect_to fiche_tag_url(@fiche_tag), notice: "Fiche tag was successfully created." }
        format.json { render :show, status: :created, location: @fiche_tag }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @fiche_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fiche_tags/1 or /fiche_tags/1.json
  def update
    respond_to do |format|
      if @fiche_tag.update(fiche_tag_params)
        format.html { redirect_to fiche_tag_url(@fiche_tag), notice: "Fiche tag was successfully updated." }
        format.json { render :show, status: :ok, location: @fiche_tag }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @fiche_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fiche_tags/1 or /fiche_tags/1.json
  def destroy
    @fiche_tag.destroy

    respond_to do |format|
      format.html { redirect_to fiche_tags_url, notice: "Fiche tag was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fiche_tag
      @fiche_tag = FicheTag.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def fiche_tag_params
      params.require(:fiche_tag).permit(:fiche_id, :tag_id)
    end
end
