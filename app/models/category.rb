class Category < ApplicationRecord
      has_many :fiche_categories
      has_many :fiches, through: :fiche_categories
end
