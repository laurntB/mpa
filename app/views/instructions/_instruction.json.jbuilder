json.extract! instruction, :id, :soumission_reglementaire, :enjeu_biodiversite, :enjeu_patrimoine, :enjeu_ressources, :synthese, :user_id, :created_at, :updated_at
json.url instruction_url(instruction, format: :json)
