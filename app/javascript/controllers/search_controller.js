import { Controller } from "@hotwired/stimulus"



function sendData(data) {
    //const formData = new FormData();
    //formData.append("flicibip_param", data);
    return fetch(
	"/map/analyse",
	{
	    method: "POST",
	    headers: {
		'Content-Type': 'application/json; charset=utf-8'
	    },
	    // Set the FormData instance as the request body
	    body: JSON.stringify(data)
	})
	.then( response => {
	    console.log(response.status);
	    if(response.status == 200){
		console.log(response.json);
		return response.json();
	    }else{
		return response.status;
	    }
	});
}


export default class extends Controller {
    static targets = ['loading','display'];

    connect(){
	var html2display;
	var data = {
	    flicibip_params:
	    {
		qry: document.querySelector("#flicibip_param").value,
		parcelle: document.querySelector("#flicibip_info").value
	    }
	};
	console.log(data);
	sendData(data).then(json=>{
	    this.loadingTarget.textContent = "";
	    console.log(json.state);
	    if(json.state == -9 ){
		html2display = "<p>La parcelle "+json.parcelle_nom+" n'a pas été trouvée dans la base, " +
		    "il s'agit probablement d'un problème de référence par rapport au millésime." +
		    " Utilisez l'interface cartographique pour une sélection plus sûre du numéro cadastral.</p>"
	    }else{
		html2display = json.report;
	    }
	    this.displayTarget.innerHTML = '<h3>Sujétions, a priori, concernant la parcelle : '+json.parcelle_nom+'</h3>'+html2display;
	});
    }
}



