json.extract! fiche_tag, :id, :fiche_id, :tag_id, :created_at, :updated_at
json.url fiche_tag_url(fiche_tag, format: :json)
