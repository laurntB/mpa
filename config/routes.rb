Rails.application.routes.draw do
  resources :fiche_categories
  resources :fiche_tags
  resources :fiches
  resources :categories
  resources :tags
  
  devise_for :users, :controllers => { registrations: 'users/registrations' }

  root 'home#index'
  get 'home/index'
  get 'home/liens'
  get 'home/private'

  get 'map' => 'map#index'
  post 'map/search'
  post 'map/analyse'

  resources :demands do
    resources :instructions, shallow: true  
  end

  resources :instructions do
    resources :endsteps
  end
  
  resources :endsteps
  
  get 'mdpages/*path' => 'mdpages#show', as: :mdpages
  
# ============================================================
# ============================================================
# ============================================================


  # get "/articles", to: "articles#index"
  # get "/articles/:id", to: "articles#show"
  # ruby bin/rails routes
  resources :articles do
    resources :comments
  end

  resources :messages
  resources :books
  resources :links
  resources :users
  
end
