class FicheCategoriesController < ApplicationController
  before_action :set_fiche_category, only: %i[ show edit update destroy ]

  # GET /fiche_categories or /fiche_categories.json
  def index
    @fiche_categories = FicheCategory.all
  end

  # GET /fiche_categories/1 or /fiche_categories/1.json
  def show
  end

  # GET /fiche_categories/new
  def new
    @fiche_category = FicheCategory.new
  end

  # GET /fiche_categories/1/edit
  def edit
  end

  # POST /fiche_categories or /fiche_categories.json
  def create
    @fiche_category = FicheCategory.new(fiche_category_params)

    respond_to do |format|
      if @fiche_category.save
        format.html { redirect_to fiche_category_url(@fiche_category), notice: "Fiche category was successfully created." }
        format.json { render :show, status: :created, location: @fiche_category }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @fiche_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fiche_categories/1 or /fiche_categories/1.json
  def update
    respond_to do |format|
      if @fiche_category.update(fiche_category_params)
        format.html { redirect_to fiche_category_url(@fiche_category), notice: "Fiche category was successfully updated." }
        format.json { render :show, status: :ok, location: @fiche_category }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @fiche_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fiche_categories/1 or /fiche_categories/1.json
  def destroy
    @fiche_category.destroy

    respond_to do |format|
      format.html { redirect_to fiche_categories_url, notice: "Fiche category was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fiche_category
      @fiche_category = FicheCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def fiche_category_params
      params.require(:fiche_category).permit(:fiche_id, :category_id)
    end
end
