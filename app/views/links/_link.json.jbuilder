json.extract! link, :id, :link, :user_id, :created_at, :updated_at
json.url link_url(link, format: :json)
