json.extract! endstep, :id, :validation, :observation, :user_id, :instruction_id, :created_at, :updated_at
json.url endstep_url(endstep, format: :json)
