class EndstepsController < ApplicationController
  
  before_action :set_endstep, only: %i[ show edit update destroy ]

  load_and_authorize_resource
  
  # GET /endsteps or /endsteps.json
  def index
    if current_user.valideur?
      @endsteps = current_user.endsteps
    end  
    if current_user.superviseur?
      @endsteps = Endstep.all
    end
  end

  # GET /endsteps/1 or /endsteps/1.json
  def show
  end

  # GET /endsteps/new
  def new
    @instruction = Instruction.find(params[:instruction_id])
    @endstep = Endstep.new()
  end

  # GET /endsteps/1/edit
  def edit
  end

  # POST /endsteps or /endsteps.json
  def create
    @endstep = Endstep.new(endstep_params) 
    @endstep.user_id = current_user.id
    @endstep.save
    redirect_to endstep_path(@endstep)
  end

  # PATCH/PUT /endsteps/1 or /endsteps/1.json
  def update
    respond_to do |format|
      if @endstep.update(endstep_params)
        format.html { redirect_to endstep_url(@endstep), notice: "Endstep was successfully updated." }
        format.json { render :show, status: :ok, location: @endstep }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @endstep.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /endsteps/1 or /endsteps/1.json
  def destroy
    @endstep.destroy

    respond_to do |format|
      format.html { redirect_to endsteps_url, notice: "Endstep was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_endstep
      @endstep = Endstep.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def endstep_params
      params.require(:endstep).permit(:validation, :observation, :instruction_id)
    end
end
