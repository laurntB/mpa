class CreateInstructions < ActiveRecord::Migration[7.0]
  def change
    create_table :instructions do |t|
      t.boolean :soumission_reglementaire
      t.text :enjeu_biodiversite
      t.text :enjeu_patrimoine
      t.text :enjeu_ressources
      t.text :synthese
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
