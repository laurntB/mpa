json.extract! fiche_category, :id, :fiche_id, :category_id, :created_at, :updated_at
json.url fiche_category_url(fiche_category, format: :json)
