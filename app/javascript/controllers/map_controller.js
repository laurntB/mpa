import jquery from "jquery" ;
import L from "leaflet";
import select2 from "select2";   // https://github.com/select2/select2/issues/6081
select2();
import { Controller } from "@hotwired/stimulus";


function config4_localgeoserver ()
{
    // "http://localhost:8080/geoserver/mq/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=mq%3Acadastre-972-parcelles&outputFormat=text%2Fjavascript&srsname=EPSG%3A4326";
    var urlparts = [
	"http://localhost:8080/geoserver/mq/ows?service=WFS",
	"version=1.0.0",
	"request=GetFeature",
	"typeName=mq%3Acadastre-972-parcelles",
	"outputFormat=text%2Fjavascript",
	"srsname=EPSG%3A4326"
    ];
    
    return {
	url: urlparts.join('&'),
	standardize_properties_function: function(p){return p;},
	attribution: '&copy; <a href="https:/:localhost:8080/geoserver">Géoserver Local</a>',
	datatype: 'jsonp' // sans ça ne marche pas !
    };
}


function config4_geoplatform()
{
    var urlparts = [
	"https://data.geopf.fr/wfs/ows?service=WFS",
	"VERSION=1.0.0", // marche pas avec 2.0.0
	"request=GetFeature",
	"typeName=BDPARCELLAIRE-VECTEUR_WLD_BDD_WGS84G%3Aparcelle",
	"outputFormat=application/json",
	"srsname=EPSG%3A4326",
	"restrictToRequestBBOX=1"
    ];


    function standardize (p)
    {
	return {
	    commune: p.code_dep+p.code_com,
	    section: p.section,
	    numero: p.numero
	}
    }
    
    return {
	url: urlparts.join('&'),
	standardize_properties_function: standardize,
	attribution: '&copy; <a href="https://geoservices.ign.fr/services-geoplateforme-diffusion">Géoplateforme</a>',
	datatype: 'json'
    };
}


function setup_map ()
{
    var config = config4_geoplatform() ; 
    // var config = config4_localgeoserver() ;
    const standardize = config.standardize_properties_function;

    
    var cadastre_visibility_min_thresold = 15;
    var zoom_default_start = 15; //10
    var geojson_layer_active = null;
    var selected_parcell = null;
    var selected_feature = null;
    var selection_layer = null;

    var map = L.map('map').setView([14.6487,-60.9988], zoom_default_start);  

    //Geojson style file
    var myStyle = {
	'color': 'fuchsia',
	'fillColor': 'fuchsia',
	'opacity': 0.3,
	'fillOpacity': 0.05,
	'weight': 1.2
    };

    var myStyleSelection = {
	'color': 'red',
	'fillColor': 'yellow',
	'opacity': 0.8,
	'fillOpacity': 0.4,
	'weight': 3
    };



    /**
     * @param {String} HTML representing a single element.
     * @param {Boolean} flag representing whether or not to trim input whitespace, defaults to true.
     * @return {Element | HTMLCollection | null}
     */
    function fromHTML(html, trim = true) {
	// Process the HTML string.
	html = trim ? html.trim() : html;
	if (!html) return null;

	// Then set up a new template element.
	const template = document.createElement('template');
	template.innerHTML = html;
	const result = template.content.children;

	// Then return either an HTMLElement or HTMLCollection,
	// based on whether the input HTML had one or more roots.
	if (result.length === 1) return result[0];
	return result;
    }

    function remove_active_layer()
    {
	if(geojson_layer_active!=null) geojson_layer_active.remove();
	geojson_layer_active = null;
    }

    function set_active_layer(layer)
    {
	geojson_layer_active = layer ;
    }
    
    function use_selection(layer){
	var selection = layer.feature;
	return {
	    leafletgeom: layer,
	    properties: selection.properties
	};
    }

    function format_map_bounds(mb){
	return mb._southWest.lng.toString()
	    +'%2C'+ mb._southWest.lat.toString()
	    +'%2C'+ mb._northEast.lng.toString()
	    +'%2C'+ mb._northEast.lat.toString() ; 
    }


    function style_path(path,style)
    {
	Object.keys(style).forEach(key=>{
	    //console.log(`Changing ${key} : ${path.getAttribute(key)} by ${style[key]}`);
	    path.setAttribute(key,style[key]);
	})
    }

    function tooltip(layer){
	var properties = standardize(layer.feature.properties);
	return `${properties.commune}-${properties.section}-${properties.numero}`;
    }


    var Parcelle = L.Polygon.extend({
	cust_properties:{},
	setProperties: function(data){
	    this.cust_properties = data;
	},
	getProperties: function(){
	    return this.cust_properties;
	}
    });


    function popup(layer){
	console.log(layer);
	var selection = use_selection(layer);
	$('#parcelle-selection-active').data('geojson',selection.leafletgeom.toGeoJSON());
	var formatted_bounds = format_map_bounds(selection.leafletgeom.getBounds()) ;
	console.log(formatted_bounds);
	$('#parcelle-selection-active').data('bounds',formatted_bounds);
	if(selection_layer!=null)selection_layer.remove();
	
	var parcelle = new Parcelle(selection.leafletgeom.getLatLngs(selection.geometry));
	parcelle.setProperties(standardize(selection.properties));
	selection_layer = L.featureGroup([parcelle]);
	selection_layer
	    .setStyle(myStyleSelection)
	    .bringToFront()
	    .addTo(map);
	
	var parcelle_info = parcelle.getProperties();
	//document.querySelector("#commune").value = parcelle_info['commune'];
	$('#commune_selector').val(parcelle_info['commune']);
	$('#commune_selector').trigger('change');
	document.querySelector("#section").value = parcelle_info['section'];
	document.querySelector("#numero").value = parcelle_info['numero'];
	document.querySelector("#carte_commune").value = parcelle_info['commune'];
	document.querySelector("#carte_section").value = parcelle_info['section'];
	document.querySelector("#carte_numero").value = parcelle_info['numero'];
	document.querySelector("#carte_geojson").value = JSON.stringify($('#parcelle-selection-active').data('geojson'));
	
	document.querySelector("#carte_bounds").value = formatted_bounds;

	return "La parcelle "
	    + `${parcelle_info.commune}-${parcelle_info.section}-${parcelle_info.numero}`
	    + " est sélectionnée.";
    }


    // the ajax callback function
    function handleJson(data) {
	remove_active_layer();
	var geojson_layer = L.geoJson(data, {
	    style: myStyle
	    /*,
	      onEachFeature: function(feature, layer) {
	      layer.bindPopup(`Name: ${feature.properties.name_of_your_property}`)
	      }*/
	});
	set_active_layer(geojson_layer);
	geojson_layer.bindTooltip(tooltip)
	    .bindPopup(popup)
	    .bringToFront()
	    .addTo(map);
    }

    map.on("moveend zoomend", function(e){
	var map_bounds = map.getBounds();
	if( map.getZoom() > cadastre_visibility_min_thresold){
	    $.ajax(config['url']+"&bbox="+format_map_bounds(map_bounds),
		   {dataType: config['datatype'],
		    jsonpCallback: 'parseResponse',
		    success: handleJson});
	}else{
	    remove_active_layer();
	}
    });


    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: [
	    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	    config['attribution']
	].join('|')
    }).addTo(map);

}


function setup_selectbox(){

  var my_data = [
      {
	  "id": "97201",
	  "text": "L'Ajoupa-Bouillon - 97201"
      },
      {
	  "id": "97202",
	  "text": "Les Anses-d'Arlet - 97202"
      },
      {
	  "id": "97203",
	  "text": "Basse-Pointe - 97203"
      },
      {
	  "id": "97204",
	  "text": "Le Carbet - 97204"
      },
      {
	  "id": "97205",
	  "text": "Case-Pilote - 97205"
      },
      {
	  "id": "97206",
	  "text": "Le Diamant - 97206"
      },
      {
	  "id": "97207",
	  "text": "Ducos - 97207"
      },
      {
	  "id": "97208",
	  "text": "Fonds-Saint-Denis - 97208"
      },
      {
	  "id": "97209",
	  "text": "Fort-de-France - 97209"
      },
      {
	  "id": "97210",
	  "text": "Le François - 97210"
      },
      {
	  "id": "97211",
	  "text": "Grand'Rivière - 97211"
      },
      {
	  "id": "97212",
	  "text": "Gros-Morne - 97212"
      },
      {
	  "id": "97213",
	  "text": "Le Lamentin - 97213"
      },
      {
	  "id": "97214",
	  "text": "Le Lorrain - 97214"
      },
      {
	  "id": "97215",
	  "text": "Macouba - 97215"
      },
      {
	  "id": "97216",
	  "text": "Le Marigot - 97216"
      },
      {
	  "id": "97217",
	  "text": "Le Marin - 97217"
      },
      {
	  "id": "97218",
	  "text": "Le Morne-Rouge - 97218"
      },
      {
	  "id": "97219",
	  "text": "Le Prêcheur - 97219"
      },
      {
	  "id": "97220",
	  "text": "Rivière-Pilote - 97220"
      },
      {
	  "id": "97221",
	  "text": "Rivière-Salée - 97221"
      },
      {
	  "id": "97222",
	  "text": "Le Robert - 97222"
      },
      {
	  "id": "97223",
	  "text": "Saint-Esprit - 97223"
      },
      {
	  "id": "97224",
	  "text": "Saint-Joseph - 97224"
      },
      {
	  "id": "97225",
	  "text": "Saint-Pierre - 97225"
      },
      {
	  "id": "97226",
	  "text": "Sainte-Anne - 97226"
      },
      {
	  "id": "97227",
	  "text": "Sainte-Luce - 97227"
      },
      {
	  "id": "97228",
	  "text": "Sainte-Marie - 97228"
      },
      {
	  "id": "97229",
	  "text": "Schœlcher - 97229"
      },
      {
	  "id": "97230",
	  "text": "La Trinité - 97230"
      },
      {
	  "id": "97231",
	  "text": "Les Trois-Îlets - 97231"
      },
      {
	  "id": "97232",
	  "text": "Le Vauclin - 97232"
      },
      {
	  "id": "97233",
	  "text": "Le Morne-Vert - 97233"
      },
      {
	  "id": "97234",
	  "text": "Bellefontaine - 97234"
      }
  ];

  
  
  $('#commune_selector').select2({
      data: my_data,
      theme: 'dsfr'
      //dropdownParent: $('#myModal')
  });
  
}



export default class extends Controller {
    connect() {
	this.element.textContent = "Vueillez zoomer jusqu'à une échelle assez "
	    +"fine pour interroger une parcelle.";
	setup_map();
	setup_selectbox();
    }
}


