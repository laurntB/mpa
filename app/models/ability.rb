# frozen_string_literal: true

class Ability
  
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    alias_action :create, :read, :update, :destroy, to: :crud

    if user.id
      if user.demandeur?
        can :manage, Demand, user_id: user.id
      end
      if user.instructeur?
        can :read, Demand
        can :manage, Instruction, user_id: user.id
      end
      if user.valideur?
        can :read, Instruction
        can :manage, Endstep, user_id: user.id
      end
      if user.superviseur?
        can :read, :all
      end
    end
    
  end
end
