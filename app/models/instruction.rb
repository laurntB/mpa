class Instruction < ApplicationRecord
  belongs_to :user
  belongs_to :demand
  has_one :endstep, dependent: :destroy
end
