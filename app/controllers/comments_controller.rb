class CommentsController < ApplicationController

  ### http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy
  
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    @comment.user_id = current_user.id ## https://stackoverflow.com/questions/54152585/how-to-add-user-id-in-form-params-in-rails-controller
    @comment.save
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article), status: :see_other
  end

  

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body, :status)
    end
end
    
