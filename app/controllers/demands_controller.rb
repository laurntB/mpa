class DemandsController < ApplicationController
  before_action :set_demand, only: %i[ show edit update destroy ]

  load_and_authorize_resource
  
  # GET /demands or /demands.json
  def index
    if current_user.demandeur?
      @demands = current_user.demands
    end
    if current_user.instructeur?
      @demands = Demand.left_outer_joins(:instruction).where("demand_id IS NULL")
    end
    if current_user.superviseur?
      @demands = Demand.all
    end
  end
 
  
  # GET /demands/1 or /demands/1.json
  def show
    
  end

  # GET /demands/new
  def new
    @demand = Demand.new
  end

  # GET /demands/1/edit
  def edit
  end

  # POST /demands or /demands.json
  def create
    @demand = Demand.new(demand_params)
    @demand.user_id = current_user.id
    respond_to do |format|
      if @demand.save
        format.html { redirect_to demand_url(@demand), notice: "Demand was successfully created." }
        format.json { render :show, status: :created, location: @demand }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @demand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /demands/1 or /demands/1.json
  def update
    respond_to do |format|
      if @demand.update(demand_params)
        format.html { redirect_to demand_url(@demand), notice: "Demand was successfully updated." }
        format.json { render :show, status: :ok, location: @demand }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @demand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /demands/1 or /demands/1.json
  def destroy
    @demand.destroy

    respond_to do |format|
      format.html { redirect_to demands_url, notice: "Demand was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_demand
      @demand = Demand.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def demand_params
      params.require(:demand).permit(:nom, :prenom, :mail, :commune, :parcelle_section, :parcelle_numero)
    end
    
    
end
