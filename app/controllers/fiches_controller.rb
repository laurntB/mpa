class FichesController < ApplicationController
  before_action :set_fiche, only: %i[ show edit update destroy ]

  # GET /fiches or /fiches.json
  def index
    @fiches = Fiche.all
  end

  # GET /fiches/1 or /fiches/1.json
  def show
  end

  # GET /fiches/new
  def new
    @fiche = Fiche.new
  end

  # GET /fiches/1/edit
  def edit
  end

  # POST /fiches or /fiches.json
  def create
    @fiche = Fiche.new(fiche_params)

    respond_to do |format|
      if @fiche.save
        format.html { redirect_to fich_url(@fiche), notice: "Fiche was successfully created." }
        format.json { render :show, status: :created, location: @fiche }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @fiche.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fiches/1 or /fiches/1.json
  def update
    respond_to do |format|
      if @fiche.update(fiche_params)
        format.html { redirect_to fiche_url(@fiche), notice: "Fiche was successfully updated." }
        format.json { render :show, status: :ok, location: @fiche }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @fiche.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fiches/1 or /fiches/1.json
  def destroy
    @fiche.destroy

    respond_to do |format|
      format.html { redirect_to fiches_url, notice: "Fiche was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fiche
      @fiche = Fiche.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def fiche_params
      params.require(:fiche).permit(:fichier_src, :title, :date, :banner, :facebook_author, :infos, :intention, :googlepdf_code, :googlepdf_oriname, :doc_pdf)
    end
end
