// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

// Other imports...
import jquery from "jquery"
window.jQuery = jquery
window.$ = jquery

import * as dsfr from "dsfr"
