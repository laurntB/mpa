class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :trackable and :omniauthable

  enum role: [:demandeur, :instructeur, :valideur, :superviseur]
  after_initialize :set_default_role, :if => :new_record?

  def set_default_role
    self.role ||= :demandeur
  end

  attr_accessor :login

  has_many :articles
  has_many :comments
  has_many :links
  has_many :demands
  has_many :instructions
  has_many :endsteps
  
  validates :username, presence: true, uniqueness: {case_sensitive: false},
            format: {with: /\A[a-zA-Z0-9 _\.]*\z/}

  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable


  def self.find_first_by_auth_conditions(warden_conditions)
    
    conditions =  warden_conditions.dup

    if login = conditions.delete(:login)
      where(conditions.to_hash)
        .where(
          "lower(username) = :value OR lower(email) = :value",
          value: login.downcase
        )
        .first
    else
      where(conditions.to_hash).first
    end
  end
  
end
