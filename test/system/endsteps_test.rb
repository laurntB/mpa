require "application_system_test_case"

class EndstepsTest < ApplicationSystemTestCase
  setup do
    @endstep = endsteps(:one)
  end

  test "visiting the index" do
    visit endsteps_url
    assert_selector "h1", text: "Endsteps"
  end

  test "should create endstep" do
    visit endsteps_url
    click_on "New endstep"

    fill_in "Instruction", with: @endstep.instruction_id
    fill_in "Observation", with: @endstep.observation
    fill_in "User", with: @endstep.user_id
    check "Validation" if @endstep.validation
    click_on "Create Endstep"

    assert_text "Endstep was successfully created"
    click_on "Back"
  end

  test "should update Endstep" do
    visit endstep_url(@endstep)
    click_on "Edit this endstep", match: :first

    fill_in "Instruction", with: @endstep.instruction_id
    fill_in "Observation", with: @endstep.observation
    fill_in "User", with: @endstep.user_id
    check "Validation" if @endstep.validation
    click_on "Update Endstep"

    assert_text "Endstep was successfully updated"
    click_on "Back"
  end

  test "should destroy Endstep" do
    visit endstep_url(@endstep)
    click_on "Destroy this endstep", match: :first

    assert_text "Endstep was successfully destroyed"
  end
end
