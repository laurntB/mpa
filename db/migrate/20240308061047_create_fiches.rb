class CreateFiches < ActiveRecord::Migration[7.0]
  def change
    create_table :fiches do |t|
      t.string :fichier_src
      t.string :title
      t.string :date
      t.string :banner
      t.string :facebook_author
      t.string :infos
      t.string :intention
      t.string :googlepdf_code
      t.string :googlepdf_oriname

      t.timestamps
    end
  end
end
