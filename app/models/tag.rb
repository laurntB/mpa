class Tag < ApplicationRecord
      has_many :fiche_tags
      has_many :fiches, through: :fiche_tags
end
