class InstructionsController < ApplicationController
  
  before_action :set_instruction, only: %i[ show edit update destroy ]

  load_and_authorize_resource
  
  def index
    if current_user.instructeur?
      @instructions = current_user.instructions
    end  
    if current_user.valideur?
      @instructions = Instruction.left_joins(:endstep).where("instruction_id IS NULL")
    end  
    if current_user.superviseur?
      @instructions = Instruction.all
    end
  end

  
  def show
  end

  
  def new
    @demand = Demand.find(params[:demand_id])
    @instruction = Instruction.new()
  end

  # GET /instructions/1/edit
  def edit
  end

  # POST /instructions or /instructions.json
  def create
    @instruction = Instruction.new(instruction_params)
    @instruction.user_id = current_user.id
    @instruction.save
    redirect_to instruction_path(@instruction)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instruction
      @instruction = Instruction.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def instruction_params
      params.require(:instruction).permit(:soumission_reglementaire, :enjeu_biodiversite, :enjeu_patrimoine, :enjeu_ressources, :synthese, :demand_id)
    end
end
