class CreateFicheTags < ActiveRecord::Migration[7.0]
  def change
    create_table :fiche_tags do |t|
      t.references :fiche, null: false, foreign_key: true
      t.references :tag, null: false, foreign_key: true

      t.timestamps
    end
  end
end
