json.extract! fich, :id, :fichier_src, :title, :date, :banner, :facebook_author, :infos, :intention, :googlepdf_code, :googlepdf_oriname, :created_at, :updated_at
json.url fich_url(fich, format: :json)
