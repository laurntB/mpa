class CreateDemands < ActiveRecord::Migration[7.0]
  def change
    create_table :demands do |t|
      t.string :nom
      t.string :prenom
      t.string :mail
      t.string :commune
      t.string :parcelle_section
      t.integer :parcelle_numero
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
