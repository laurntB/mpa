# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "@hotwired/turbo-rails", to: "turbo.min.js", preload: true
pin "@hotwired/stimulus", to: "stimulus.min.js", preload: true
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true

pin "jquery" # @3.7.1
pin "leaflet" # @1.9.4
pin "select2" # @4.0.13

pin "dsfr", to: "dsfr.module.min.js"

pin_all_from "app/javascript/controllers", under: "controllers"
