class Fiche < ApplicationRecord
  
  has_many :fiche_categories
  has_many :categories, through: :fiche_categories
  has_many :fiche_tags
  has_many :tags, through: :fiche_tags

  has_one_attached :doc_pdf
  
  def pdf_url
    if self.doc_pdf.attached?
      ## https://stackoverflow.com/questions/50424251/how-can-i-get-url-of-my-attachment-stored-in-active-storage-in-my-rails-controll
      Rails.application.routes.url_helpers.rails_blob_url(
        self.doc_pdf.blob,
        only_path: true   ## Rails.application.config.action_mailer.default_url_options
      ).to_s
    else
      "https://drive.google.com/file/d/#{self.googlepdf_code}"
    end
  end

  def target_style
    if self.doc_pdf.attached?
      ""
    else
      'target="_blank"'.html_safe
    end
  end
  
end
