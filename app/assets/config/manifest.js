//= link          dsfr.main.min.css
//= link          dsfr.nomodule.min.js
//= link          dsfr.module.min.js
//= link_directory ../../../vendor/assets/dsfr/scheme
//= link_directory ../../../vendor/assets/dsfr/legacy
//= link_directory ../../../vendor/assets/dsfr/core 
//= link_tree      ../../../vendor/assets/dsfr/component
//= link_directory ../../../vendor/assets/dsfr/favicon
//= link_tree      ../../../vendor/assets/dsfr/artwork
//= link_tree      ../../../vendor/assets/dsfr/icons
//= link_directory ../../../vendor/assets/dsfr/utility .css

//= link_tree ../images
//= link_tree ../../javascript .js
//= link_tree ../../../vendor/javascript .js
//= link_tree ../builds
//= link select2.css
//= link carousel.css
