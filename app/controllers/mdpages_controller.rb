
class MdpagesController < ApplicationController
  
  def show
    parser = RubyMatter.parse(self.get_markdown)
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, footnotes: true)
    @frontmatter = parser.data
    @html_content = markdown.render(parser.content).html_safe
  end
  
  private

  def path_to_md_source
    Path(Rails.root,'markdown_pages',params[:path]).add_ext('md')
  end

  def get_markdown
    File.read(self.path_to_md_source)
  end
end
