require "test_helper"

class EndstepsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @endstep = endsteps(:one)
  end

  test "should get index" do
    get endsteps_url
    assert_response :success
  end

  test "should get new" do
    get new_endstep_url
    assert_response :success
  end

  test "should create endstep" do
    assert_difference("Endstep.count") do
      post endsteps_url, params: { endstep: { instruction_id: @endstep.instruction_id, observation: @endstep.observation, user_id: @endstep.user_id, validation: @endstep.validation } }
    end

    assert_redirected_to endstep_url(Endstep.last)
  end

  test "should show endstep" do
    get endstep_url(@endstep)
    assert_response :success
  end

  test "should get edit" do
    get edit_endstep_url(@endstep)
    assert_response :success
  end

  test "should update endstep" do
    patch endstep_url(@endstep), params: { endstep: { instruction_id: @endstep.instruction_id, observation: @endstep.observation, user_id: @endstep.user_id, validation: @endstep.validation } }
    assert_redirected_to endstep_url(@endstep)
  end

  test "should destroy endstep" do
    assert_difference("Endstep.count", -1) do
      delete endstep_url(@endstep)
    end

    assert_redirected_to endsteps_url
  end
end
