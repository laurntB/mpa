json.extract! tag, :id, :libelle, :created_at, :updated_at
json.url tag_url(tag, format: :json)
