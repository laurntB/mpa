json.extract! message, :id, :title, :message, :image, :user_id, :created_at, :updated_at
json.url message_url(message, format: :json)
json.image do
  json.array!(message.image) do |image|
    json.id image.id
    json.url url_for(image)
  end
end
