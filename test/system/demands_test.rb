require "application_system_test_case"

class DemandsTest < ApplicationSystemTestCase
  setup do
    @demand = demands(:one)
  end

  test "visiting the index" do
    visit demands_url
    assert_selector "h1", text: "Demands"
  end

  test "should create demand" do
    visit demands_url
    click_on "New demand"

    fill_in "Commune", with: @demand.commune
    fill_in "Mail", with: @demand.mail
    fill_in "Nom", with: @demand.nom
    fill_in "Parcelle numero", with: @demand.parcelle_numero
    fill_in "Parcelle section", with: @demand.parcelle_section
    fill_in "Prenom", with: @demand.prenom
    fill_in "User", with: @demand.user_id
    click_on "Create Demand"

    assert_text "Demand was successfully created"
    click_on "Back"
  end

  test "should update Demand" do
    visit demand_url(@demand)
    click_on "Edit this demand", match: :first

    fill_in "Commune", with: @demand.commune
    fill_in "Mail", with: @demand.mail
    fill_in "Nom", with: @demand.nom
    fill_in "Parcelle numero", with: @demand.parcelle_numero
    fill_in "Parcelle section", with: @demand.parcelle_section
    fill_in "Prenom", with: @demand.prenom
    fill_in "User", with: @demand.user_id
    click_on "Update Demand"

    assert_text "Demand was successfully updated"
    click_on "Back"
  end

  test "should destroy Demand" do
    visit demand_url(@demand)
    click_on "Destroy this demand", match: :first

    assert_text "Demand was successfully destroyed"
  end
end
