

class Users::RegistrationsController < Devise::RegistrationsController
  # Override the action you want here.
  before_action :configure_sign_up_params, only: [:create]


  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up){ |u|
      u.permit(:username, :email, :password, :password_confirmation)
    }
  end
end
