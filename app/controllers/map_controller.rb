

class MapController < ApplicationController

  protect_from_forgery with: :null_session

  # Affiche le composant cartographique
  # leaflet, voir app/javascript/controllers/map_controller.js
  def index
  end

  
  # Mets en place la page qui va permettre de dérouler la requête asynchrone pilotée
  # par le navigateur client, adressée à la méthode 'analyse'
  # POST receiver
  def search
    qp =self.setup_search_query_param parcelle_params
    @flicibip_param = qp[:param]
    @flicibip_info = qp[:info]
    render 'search', status: :unprocessable_entity, layout: 'layouts/application'
  end


  
  # Traitement potentiellement long
  # POST receiver
  def analyse
    process_info = self.parse_query_dispatch_to_process parcelle_params
    cmd = process_info[:cmd]
    arg = process_info[:arg]
    parcelle = process_info[:parcelle]
    response = nil
    print "Lancement appel aux services exérieurs (flicibip) ... "
    case cmd
    when "discoror_byRgjsn" then
      response = Flicibip.discoror_byRgjsn arg
    when "discoror_byParcref" then
      response = Flicibip.discoror_byParcref arg
    end
    puts  "fin"
    p response
    render json: {
             state: response[:status],
             parcelle_nom: parcelle, 
             report: response[:report]
           }
  end

  
  private
  
  # Aucun filtre sur les valeurs reçues par POST  
  def parcelle_params
    ActionController::Parameters.permit_all_parameters = true
    params
  end

  def setup_search_query_param parcelle_params
    retval = {}
    parcelle = ParcelleName.new(
      parcelle_params['commune_selector'],
      parcelle_params['section'],
      parcelle_params['numero']
    )
    retval[:info] = parcelle.as_str
    parcelle_carte  = ParcelleName.new(
      parcelle_params['carte_commune'],
      parcelle_params['carte_section'],
      parcelle_params['carte_numero']
    )
    if parcelle == parcelle_carte && parcelle_params['carte_geojson']
      retval[:param] = "G-"+parcelle_params['carte_geojson']
    else
      retval[:param] = "P-"+retval[:info]
    end
    retval
  end

  def parse_query_dispatch_to_process parcelle_params
    inputparams = parcelle_params[:flicibip_params]
    qry_type = inputparams['qry'][0..1]
    qry = inputparams['qry'][2..]
    cmd_to_run = ""
    case qry_type        
    when 'G-' then 
      cmd_to_run = "discoror_byRgjsn"
      oqry = JSON.parse(qry)
    when 'P-' then
      cmd_to_run = "discoror_byParcref"
      oqry = qry
    end
    {cmd: cmd_to_run, arg: oqry, parcelle: inputparams['parcelle']}
  end
  
end
