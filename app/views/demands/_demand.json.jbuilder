json.extract! demand, :id, :nom, :prenom, :mail, :commune, :parcelle_section, :parcelle_numero, :user_id, :created_at, :updated_at
json.url demand_url(demand, format: :json)
